use sea_orm::{prelude::*, ActiveValue, DatabaseConnection};
use std::{collections::HashMap, path::PathBuf};

use models::entities::region;

#[derive(clap::Subcommand)]
pub enum RegionCmd {
    BulkUpload { file_path: PathBuf },
}

pub async fn bulk_upload_json(
    db: DatabaseConnection,
    file_path: PathBuf,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let region_name_to_flag_map: HashMap<String, String> =
        serde_json::from_str(&std::fs::read_to_string(file_path)?)?;

    region::Entity::insert_many(region_name_to_flag_map.into_iter().map(|(region, flag)| {
        dbg!(&region, &flag, flag.len());
        region::ActiveModel {
            id: ActiveValue::NotSet,
            name: ActiveValue::Set(region),
            flag: ActiveValue::Set(flag),
        }
    }))
    .exec(&db)
    .await?;

    Ok(())
}
