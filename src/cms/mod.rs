use self::region::RegionCmd;

pub mod region;

#[derive(clap::Subcommand)]
pub enum CmsCmd {
    #[clap(subcommand)]
    Region(RegionCmd),
}
