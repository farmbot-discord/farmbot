use chrono::DateTime;
use chrono::Utc;
use std::collections::{HashMap, HashSet};

pub(crate) struct UserData {
    discord_user_id: String,
    region: Region,
    total_commands_used: u64,
    money: u64,
    farm: Vec<Id<Plot>>,
    unlocked_seeds: HashMap<Id<Crop>, UserSeedInfo>,
    market_requests: HashMap<Id<Farmer>, MarketRequest>,
    farmers: HashMap<Id<Farmer>, Farmer>,
    market_refresh_timeout: DateTime<Utc>,
    market_requests_remaining_this_hour: u8,
}

pub(crate) struct Region {
    name: String,
    code: [char; 2],
}

pub(crate) struct UserSeedInfo {
    level: u64,
    amount: u64,
}

pub(crate) struct Farmer {
    name: String,
    gender: Gender,
    unlockable_crop: Id<Crop>,
    wealth: u64,
    preferences: FarmerPreferences,
    // REVIEW(benluelo): what is this field? exp or level?
    level: u64,
    unlock_level: u8,
}

pub(crate) struct FarmerPreferences {
    color: HashSet<Id<CropColour>>,
    taste: HashSet<Id<CropFlavour>>,
}

pub(crate) struct CropColour(String);

pub(crate) struct CropFlavour(String);

pub(crate) enum Gender {
    Male,
    Female,
}

pub(crate) struct Crop {
    emoji: char,
    color: Id<CropColour>,
    flavours: Vec<Id<CropFlavour>>,
}

pub(crate) struct MarketRequest {
    /// crop -> amount
    want: HashMap<Id<Crop>, u32>,
    reward: u64,
    reputation: u64,
}

pub(crate) struct Plot {
    crop: Option<Id<Crop>>,
    planted_at: DateTime<Utc>,
    fertilized_at: DateTime<Utc>,
    watered_at: DateTime<Utc>,
}

pub struct Id<T>(i64, std::marker::PhantomData<fn() -> T>);
