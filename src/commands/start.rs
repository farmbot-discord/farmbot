use models::entities::region;
use sea_orm::*;
use twilight_http::client::InteractionClient;
use twilight_interactions::command::{AutocompleteValue, CommandModel, CreateCommand};
use twilight_model::{
    application::command::CommandOptionChoice,
    gateway::payload::incoming::InteractionCreate,
    http::interaction::{InteractionResponse, InteractionResponseData, InteractionResponseType},
};

#[derive(CommandModel)]
#[command(autocomplete = true)]
pub struct StartCommandModel {
    region: AutocompleteValue<String>,
}

impl StartCommandModel {
    pub async fn run(
        self,
        db: DatabaseConnection,
        interaction_client: InteractionClient<'_>,
        interaction: InteractionCreate,
    ) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
        dbg!(&interaction);
        match self.region {
            AutocompleteValue::None => {
                // not sure yet
                Ok(())
            }
            AutocompleteValue::Focused(region_name_focused) => {
                dbg!(&region_name_focused);
                let choices = region::Entity::find()
                    .select_only()
                    .column(region::Column::Name)
                    .filter(region::Column::Name.starts_with(&region_name_focused))
                    .all(&db)
                    .await?
                    .into_iter()
                    .take(25)
                    .map(|model| CommandOptionChoice::String {
                        name: model.name.clone(),
                        value: model.name,
                        name_localizations: None,
                    })
                    .collect();

                let _response = interaction_client
                    .create_response(
                        interaction.id,
                        &interaction.token,
                        &InteractionResponse {
                            kind: InteractionResponseType::ApplicationCommandAutocompleteResult,
                            data: Some(InteractionResponseData {
                                choices: Some(choices),
                                ..Default::default()
                            }),
                        },
                    )
                    .exec()
                    .await?;
                Ok(())
            }
            AutocompleteValue::Completed(region_name) => {
                dbg!(region_name);
                // start farming in said region if it's a valid region
                Ok(())
            }
        }
    }
}

#[derive(CreateCommand)]
#[command(name = "start", desc = "Start your farming journey!")]
pub struct StartCreateCommand {
    #[command(autocomplete = true, desc = "Where in the world you want to farm")]
    region: AutocompleteValue<String>,
}
