use clap::Parser;
use cms::{
    region::{bulk_upload_json, RegionCmd},
    CmsCmd,
};
use futures::stream::StreamExt;
use sea_orm::{Database, DatabaseConnection};
use std::{env, error::Error, ops::Deref, sync::Arc};
use twilight_cache_inmemory::{InMemoryCache, ResourceType};
use twilight_gateway::{
    cluster::{Cluster, ShardScheme},
    Event, Intents,
};
use twilight_http::Client as HttpClient;
use twilight_interactions::command::{CommandModel, CreateCommand};
use twilight_model::{
    self,
    application::interaction::InteractionData,
    id::{marker::ApplicationMarker, Id},
};

use commands::start::{StartCommandModel, StartCreateCommand};

pub mod cms;
pub(crate) mod commands;

#[derive(clap::Parser)]
pub struct Args {
    #[clap(subcommand)]
    pub cmd: Cmd,
}

#[derive(clap::Subcommand)]
pub enum Cmd {
    #[clap(subcommand)]
    Cms(CmsCmd),
    Bot(BotCmd),
}

#[derive(clap::Parser)]
pub struct BotCmd {}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    dotenvy::dotenv().ok();

    let db = Database::connect(env::var("DATABASE_URL").unwrap())
        .await
        .unwrap();

    match Args::parse().cmd {
        Cmd::Cms(cms) => match cms {
            CmsCmd::Region(RegionCmd::BulkUpload { file_path }) => {
                bulk_upload_json(db.clone(), file_path).await?;
                return Ok(());
            }
        },
        Cmd::Bot(_) => {}
    }

    pretty_env_logger::init();

    let token = env::var("DISCORD_TOKEN")?;

    // Start a single shard.
    let scheme = ShardScheme::Range {
        from: 0,
        to: 0,
        total: 1,
    };

    // Specify intents requesting events about things like new and updated
    // messages in a guild and direct messages.
    let intents = Intents::GUILD_MESSAGES | Intents::DIRECT_MESSAGES;

    let (cluster, mut events) = Cluster::builder(token.clone(), intents)
        .shard_scheme(scheme)
        .build()
        .await?;

    let cluster = Arc::new(cluster);

    // Start up the cluster
    let cluster_spawn = cluster.clone();

    tokio::spawn(async move {
        cluster_spawn.up().await;
    });

    // The http client is seperate from the gateway, so startup a new
    // one, also use Arc such that it can be cloned to other threads.
    let http_client = Arc::new(HttpClient::new(token));

    let application_id = http_client
        .current_user_application()
        .exec()
        .await?
        .model()
        .await?
        .id;

    let interaction_client = http_client.interaction(application_id);

    interaction_client
        .set_guild_commands(
            Id::new(668642213626118164),
            &[StartCreateCommand::create_command().into()],
        )
        .exec()
        .await
        .unwrap();

    // Since we only care about messages, make the cache only process messages.
    let cache = InMemoryCache::builder()
        .resource_types(ResourceType::MESSAGE)
        .build();

    // Startup an event loop to process each event in the event stream as they
    // come in.
    while let Some((shard_id, event)) = events.next().await {
        // Update the cache.
        cache.update(&event);

        // Spawn a new task to handle the event
        tokio::spawn(handle_event(
            shard_id,
            event,
            db.clone(),
            http_client.clone(),
            application_id,
        ));
    }

    Ok(())
}

async fn handle_event(
    shard_id: u64,
    event: Event,
    db: DatabaseConnection,
    http: Arc<HttpClient>,
    application_id: Id<ApplicationMarker>,
    // interaction_client: Arc<InteractionClient<'_>>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    match event {
        Event::MessageCreate(msg) if msg.content == "!ping" => {
            http.create_message(msg.channel_id)
                .content("Pong!")?
                .exec()
                .await?;
        }
        Event::ShardConnected(_) => {
            println!("Connected on shard {}", shard_id);
        }
        Event::InteractionCreate(interaction) => match &interaction.data {
            Some(InteractionData::ApplicationCommand(data)) => {
                match &*data.name {
                    "start" => {
                        StartCommandModel::from_interaction(data.deref().clone().into())
                            .unwrap()
                            .run(db, http.interaction(application_id), *interaction)
                            .await?
                    }
                    name => panic!("unknown command {name}"),
                };
            }
            _ => {}
        },
        _ => {}
    }

    Ok(())
}
